FROM condaforge/mambaforge

ENV PATH="${PATH}:/root/.pixi/bin:$PATH"

RUN apt-get update && apt-get install -y --no-install-recommends curl \
    && curl -fsSL https://pixi.sh/install.sh | bash \
    && apt-get clean && rm -rf /var/lib/apt/lists/*
